#include <jailhouse/types.h>
#include <jailhouse/cell-config.h>
struct {
	struct jailhouse_system header;
	__u64 cpus[1];
	struct jailhouse_memory mem_regions[4];
	struct jailhouse_irqchip irqchips[1];
	struct jailhouse_pci_device pci_devices[11];
	struct jailhouse_pci_capability pci_caps[81];
} __attribute__((packed)) config = {
.header = {
	.signature = JAILHOUSE_SYSTEM_SIGNATURE,
	.revision = JAILHOUSE_CONFIG_REVISION,
	.flags = JAILHOUSE_SYS_VIRTUAL_DEBUG_CONSOLE,
	.hypervisor_memory = {
		.phys_start = 0x2104000000,
		.size = 0x1000000,
	},
	.debug_console = {
		.address = 0x28001000, 
		.size = 0x1000, 
		.type = JAILHOUSE_CON_TYPE_PL011, 
		.flags = JAILHOUSE_CON_ACCESS_MMIO|JAILHOUSE_CON_REGDIST_4,
	},
	.platform_info = {
		.pci_mmconfig_base = 0x0000000040000000, 
		.pci_mmconfig_end_bus = 0xFF, 
		.pci_is_virtual = 1, 
		.pci_domain = 1,
		.arm = {
			.gic_version = 3,
			.gicd_base = 0x0000000029A00000,
			.gicr_base = 0x0000000029B00000,
			.gicc_base = 0x0000000029C00000,
			.gich_base = 0x0000000029C10000,
			.gicv_base = 0x0000000029C20000,
			.maintenance_irq = 25,
		},
	},
	.root_cell = {
		.name = "RootCell",
		.cpu_set_size = sizeof(config.cpus),
		.num_memory_regions = ARRAY_SIZE(config.mem_regions),
		.num_irqchips = ARRAY_SIZE(config.irqchips),
		.num_pci_devices = ARRAY_SIZE(config.pci_devices),
		.num_pci_caps = ARRAY_SIZE(config.pci_caps),
		.vpci_irq_base = 100,
	},
},
.cpus = { 0xff, },
.mem_regions = {
	{.phys_start=0x28000000, .virt_start=0x28000000, .size=0x58000000, .flags=JAILHOUSE_MEM_READ | JAILHOUSE_MEM_WRITE | JAILHOUSE_MEM_IO},
	{.phys_start=0x80000000, .virt_start=0x80000000, .size=0x7c000000, .flags=JAILHOUSE_MEM_READ | JAILHOUSE_MEM_WRITE | JAILHOUSE_MEM_EXECUTE},
	{.phys_start=0x1000000000, .virt_start=0x1000000000, .size=0x1000000000, .flags=JAILHOUSE_MEM_READ | JAILHOUSE_MEM_WRITE | JAILHOUSE_MEM_IO},
	{.phys_start=0x2000000000, .virt_start=0x2000000000, .size=0x180000000, .flags=JAILHOUSE_MEM_READ | JAILHOUSE_MEM_WRITE | JAILHOUSE_MEM_EXECUTE},
},
.irqchips = {
	{
		.address = 0x0000000029A00000, 
		.pin_base = 32, 
		.pin_bitmap = {0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff}, 
	},
},
.pci_devices = {

	/* PCIDevice: 0000:00:00.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x0, .caps_start = 0, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010000000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:00:01.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x8, .caps_start = 8, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010100000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:00:02.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x10, .caps_start = 16, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010200000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:00:03.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x18, .caps_start = 24, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010300000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:00:04.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x20, .caps_start = 32, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010400000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:00:05.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_BRIDGE, .domain = 0x0, .bdf = 0x28, .caps_start = 40, .num_caps = 8,
		.num_msi_vectors = 32, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 1, .msix_region_size = 0x1000, .msix_address = 0x1010500000,
		.bar_mask={0xfff00000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:02:00.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_DEVICE, .domain = 0x0, .bdf = 0x200, .caps_start = 48, .num_caps = 8,
		.num_msi_vectors = 1, .msi_64bits = 1, .msi_maskable = 0,
		.num_msix_vectors = 0, .msix_region_size = 0x0, .msix_address = 0x0,
		.bar_mask={0xf0000000, 0xffffffff, 0xfffc0000, 0xffffffff, 0xffffff00, 0x00000000, },
	},

	/* PCIDevice: 0000:02:00.1 */
	{
		.type = JAILHOUSE_PCI_TYPE_DEVICE, .domain = 0x0, .bdf = 0x201, .caps_start = 56, .num_caps = 6,
		.num_msi_vectors = 1, .msi_64bits = 1, .msi_maskable = 0,
		.num_msix_vectors = 0, .msix_region_size = 0x0, .msix_address = 0x0,
		.bar_mask={0xffffc000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:03:00.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_DEVICE, .domain = 0x0, .bdf = 0x300, .caps_start = 62, .num_caps = 5,
		.num_msi_vectors = 1, .msi_64bits = 0, .msi_maskable = 0,
		.num_msix_vectors = 0, .msix_region_size = 0x0, .msix_address = 0x0,
		.bar_mask={0xfffffff8, 0xfffffffc, 0xfffffff8, 0xfffffffc, 0xffffffe0, 0xfffff800, },
	},

	/* PCIDevice: 0000:04:00.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_DEVICE, .domain = 0x0, .bdf = 0x400, .caps_start = 67, .num_caps = 8,
		.num_msi_vectors = 8, .msi_64bits = 1, .msi_maskable = 1,
		.num_msix_vectors = 16, .msix_region_size = 0x1000, .msix_address = 0x58202000,
		.bar_mask={0xffffc000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},

	/* PCIDevice: 0000:06:00.0 */
	{
		.type = JAILHOUSE_PCI_TYPE_DEVICE, .domain = 0x0, .bdf = 0x600, .caps_start = 75, .num_caps = 6,
		.num_msi_vectors = 8, .msi_64bits = 1, .msi_maskable = 0,
		.num_msix_vectors = 8, .msix_region_size = 0x1000, .msix_address = 0x58301000,
		.bar_mask={0xffffe000, 0xffffffff, 0x00000000, 0x00000000, 0x00000000, 0x00000000, },
	},
},
.pci_caps = {

	/* PCIDevice: 0000:00:00.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:00:01.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:00:02.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:00:03.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:00:04.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:00:05.0 */ 
	{.id=PCI_CAP_ID_EXP, .start=0x80, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xd0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xe0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_PM, .start=0xf8, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x110, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x200, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x300, .len=16, .flags=0},

	/* PCIDevice: 0000:02:00.0 */ 
	{.id=PCI_CAP_ID_VNDR, .start=0x48, .len=2, .flags=0},
	{.id=PCI_CAP_ID_PM, .start=0x50, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_EXP, .start=0x58, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xa0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x150, .len=64, .flags=0},
	{.id=PCI_EXT_CAP_ID_REBAR, .start=0x200, .len=2, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x270, .len=16, .flags=0},

	/* PCIDevice: 0000:02:00.1 */ 
	{.id=PCI_CAP_ID_VNDR, .start=0x48, .len=2, .flags=0},
	{.id=PCI_CAP_ID_PM, .start=0x50, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_EXP, .start=0x58, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0xa0, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_VNDR, .start=0x100, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x150, .len=64, .flags=0},

	/* PCIDevice: 0000:03:00.0 */ 
	{.id=PCI_CAP_ID_PM, .start=0x40, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0x50, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_EXP, .start=0x70, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_SATA, .start=0xe0, .len=2, .flags=0},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x100, .len=64, .flags=0},

	/* PCIDevice: 0000:04:00.0 */ 
	{.id=PCI_CAP_ID_PM, .start=0x40, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0x50, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_EXP, .start=0x70, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0xb0, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x100, .len=64, .flags=0},
	{.id=PCI_CAP_ID_EXP, .start=0x158, .len=16, .flags=0},
	{.id=PCI_EXT_CAP_ID_LTR, .start=0x178, .len=8, .flags=0},
	{.id=PCI_EXT_CAP_ID_L1SS, .start=0x180, .len=2, .flags=0},

	/* PCIDevice: 0000:06:00.0 */ 
	{.id=PCI_CAP_ID_PM, .start=0x50, .len=8, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSI, .start=0x70, .len=10, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_MSIX, .start=0x90, .len=12, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_CAP_ID_EXP, .start=0xa0, .len=20, .flags=JAILHOUSE_PCICAPS_WRITE},
	{.id=PCI_EXT_CAP_ID_ERR, .start=0x100, .len=64, .flags=0},
	{.id=PCI_EXT_CAP_ID_LTR, .start=0x150, .len=8, .flags=0},
},
};
