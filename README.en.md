# 1 概述

Jailhouse是一个基于Linux的分区虚拟机管理器，一旦激活就完全控制硬件。Jailhouse使用被称为cell的方式来配置CPU等硬件的虚拟化特性，cell是系统硬件资源的描述，使用C语言语法来描述。Cell分为root cell和non-root cell，root cell接管系统硬件资源，只有一个；non-root cell可以有多个，并且从root cell中获取系统资源，可独占或与root cell共享。

Jailhouse编译完成后，生成文件分为三部分：第一部分，Jailhouse驱动和hypervisor固件，这部分提供用户态接口并初始化hypervisor；第二部分，cell和guest镜像，cell是镜像运行所需的系统资源的描述；guest镜像运行在cell之上，包括裸机，RTOS和Linux内核镜像等；第三部分，用户态工具，通过这些工具加载cell，运行镜像，查看Jailhouse状态等。


# 2 编译

在x86环境下编译Jailhouse,需配置好如下aarch64交叉编译环境，编译python脚本需要python-pip和python-mako软件包，需要注意交叉编译环境和目标机环境的python版本应保持一致：

	$ export PATH=/opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin:$PATH
	$ export ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu-

之后通过环境变量KDIR指定到内核源码树目录，DESTDIR指定编译安装目录，最后使用make install命令编译安装,本例将安装目录定义在用户目录下，~/jailhouse-bin目录：
	
	$ make DESTDIR=~/jailhouse-bin KDIR=~/linux_dir install

将安装目录下的子目录拷贝到开发板的根目录

	$ cp -r ~/jailhouse-bin/* /
	$ cp -r ~/jailhouse-bin/home/<user>/.local/usr/local/lib/* /usr/local/lib

编译后的cell文件在jailhouse code的configs/arm64目录下，裸机程序在jailhousecode的inmates/demos/arm64目录下。


# 3 运行

目前在飞腾FT2000/4、D2000和E2000上运行如下示例，测试内核版本为4.19，根文件系统为Ubuntu 18.04。如有需要，可联系飞腾嵌入式软件部获取。

## 3.1 设备树保留内存节点

Jailhouse需要保留部分内存空间，在设备树中添加如下节点，保留从0xb0000000开始的256M内存，需要注意这部分内存应与root cell配置文件中的基地址保持一致。

    reserved-memory {
        #address-cells = <0x00000002>;
        #size-cells = <0x00000002>;
        ranges;
        reserved@b0000000 {
            reg = <0x00000000 0xb0000000 0x00000000 0x10000000>;
            no-map;
        };
    };  

## 3.2 裸机程序

### 3.2.1 ft2000测试
首先加载jailhouse驱动模块，再加载root cell和non-root cell，在FT2000/4板卡上，ft2004.cell为root cell，主要接管整个系统，ft2004-inmate-demo.cell为non-root cell，从root cell中获取一部分系统资源（共享或者独占），分配给后续加载的裸机程序；最后加载并运行的裸机程序，命令序列和运行效果如下图所示。

	# modprobe jailhouse;
	# jailhouse enable ft2004-dev.cell;
	# jailhouse cell create ft2004-freertos-basic.cell;
	# jailhouse cell load 1 uart-demo.bin;
	# jailhouse cell start 1;
